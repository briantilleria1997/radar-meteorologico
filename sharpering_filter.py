import numpy as np

def sharpering_filter(V, L, C_v, alpha):
    """
      ================================INPUTS===================================
    
    V          : señal de radar meteorológico sobremuestrado en rango.

    L          : factor de oversampling.
    
    C_v        : matriz Toepliz normalizada de correlación.

    alpha      : valor que ajusta el nivel de blanqueo deseado, varía entre 0 (Filtro adaptado) y 1 (Blanqueo puro).


    ==============================OUTPUTS===============================
    
    X_SH       : señal de radar meteorólogico pseudo-blanqueada.
    
    """

    # Algoritmo Sharpering Filter 

    # Descomponemos C_v y encontramos la matriz de blanqueo
    
    H= np.linalg.cholesky(C_v) # Descomposición de Cholesky

    # Descomponemos en valores singulares H
    
    Q, S_may, P= np.linalg.svd(H, full_matrices=True)
 
    g=0 

    for i in range(L):
        g+= S_may[i]**4 /((alpha* S_may[i]**2 + (1-alpha))**(2))

    gamma_sh= np.sqrt(L*1/g) 

    W_SH= gamma_sh*np.linalg.inv(alpha*H + (1-alpha)*np.linalg.inv(np.conjugate(np.transpose(H))))

    X_SH= np.dot(W_SH,V)

    # Salida

    return(X_SH, gamma_sh)