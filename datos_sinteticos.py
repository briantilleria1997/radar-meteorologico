import numpy as np


def datos_sinteticos(M, L, F, p_l, h_l, S, Pn, v_mean, sigma_v, lamb, v_amax):
    """

    ================================INPUTS===================================
    M          : número de pulsos

    L          : número de muestras en range-time (factor de oversampling)

    F          : largo de la respuesta impulsional del filtro receptor

    p_l        : envolvente del pulso transmitido

    h_l        : respuesta impulsional del receptor

    S      : potencia media del a señal

    Pn      : potencia media del ruido (AWN)

    v_mean     : velocidad media Doppler

    sigma_v    : ancho de banda Doppler

    lamb       : longitud de onda del Radar

    v_amax      : máx velocidad no ambigüa 

    ==============================OUTPUTS===============================
    
    V_ln       : señal de radar meteorológico sobremuestreada en rango
    """ 
    
    # Expresamos las potencias en veces

    S= 10**((S-30)/10)
    Pn= 10**((Pn-30)/10)

    # Genero la la contribución de voltajes sq, IID CN(0,1).

    sq= np.random.normal(0, 1/np.sqrt(2), size=(2*L+F-1, M)) + \
   1j*np.random.normal(0, 1/np.sqrt(2), size=(2*L+F-1, M)) 

    # Generamos la correlación en rango

    V_1= np.zeros((L, M),dtype = 'complex_')

    for j in range(M):
        for i in range(L):
         V_1[i,j]= np.dot(sq[i:i+L,j],np.flip(p_l))

    # Convolucionamos con el receptor (importante para el caso de receptor no ideal)

    V_2= np.zeros((L+F-1,M), dtype= 'complex_') # Tiene el largo de V1 + H_l - 1

    for k in range(M):

            V_2[:,k]= np.convolve( V_1[:,k], h_l, "full")

    # Hago un reshape para borrar el transitorio y me quedo con los L valores de interes

    V_2= V_2[-L:]

    # Potencia media de la señal luego de agregar correlación en rango

    G= sum(np.absolute(np.convolve(h_l, p_l, 'full'))**2)

    # Generamos la correlación en tiempo, coloreo de espectro.

    k= np.arange(-M, 2*M, 1, dtype=int) 

    v_k= -v_amax + 2*k*v_amax/M # Vector que recorre desde -3Vamax a 3Vamax, para el aliasing

    Pow_sepec= S*2*v_amax/(np.sqrt(2*np.pi)*sigma_v*G) *np.exp((-1*(v_k-v_mean)**2)/(2*sigma_v**2)) # Espectro Gaussiano

    # Efecto de aliasing

    Pw_spec = np.zeros(v_k.shape)

    for ii in range(3):
        Pw_spec += np.roll(Pow_sepec, M*(ii-4))


    Pw= Pw_spec[2*M:3*M]  # intervalo de Nyquist, de -Vamb a Vamb

    Pw_sqrt= np.sqrt(Pw[::-1]) # Hago un flip para cambiar al eje de frecuencias.

    # Reshape del vector
    Pw_sqrt1= Pw_sqrt[int(M/2):] 
    Pw_sqrt2= Pw_sqrt[0:int(M/2)] 

    Pw_sqrt= np.concatenate((Pw_sqrt1,Pw_sqrt2),axis= None)

    # Aplico la Transformada de Fourier a los datos con correlación en rango y coloreo.

    V_2_f= np.zeros((L, M), dtype = 'complex_')
    V= np.zeros((L, M), dtype = 'complex_')

    for j in range(L):
      V_2_f [j, :] = np.fft.fft(V_2[j, :])


    for j in range(L):    
     V[j, :]= np.fft.ifft( V_2_f[j,:] * Pw_sqrt)  # V tiene la correlación en rango y tiempo.


    # Agregamos ruido de potencia Pn

    V += (   np.random.normal(0, 1, (L,M)) + \
          1j*np.random.normal(0, 1, (L,M)) )*np.sqrt(Pn/2)
    
    # Salida
    return(V)
