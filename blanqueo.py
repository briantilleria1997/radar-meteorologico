import numpy as np

def blanqueo(V, L, p_l, h_l):
    """
     ================================INPUTS===================================
    
    V          : señal de radar meteorológico sobremuestrado en rango.

    L          : factor de oversampling.
    
    p_l        : envolvente del pulso transmitido.

    h_l        : respuesta impulsional del receptor.

    ==============================OUTPUTS===============================
    
    X          : señal de radar meteorólogico sobremuestreado en rango blanqueada.
    C_v        : matriz Toepliz normalizada de correlación.
    """

    # Algoritmo de blanqueo

    ro= np.zeros(L)
    pm= np.convolve(p_l, h_l, "full") # Pm es la convolución de p_l con h_l
    ro= np.convolve(pm,np.conjugate(np.flip(pm)), "full")/(np.sum(pm**2)) # Coeficientes de correlación

    # Encuentro la matriz de correlación C_v^R

    U= np.zeros((L, L), dtype= 'complex_')
    U_conj= np.zeros((L, L), dtype= 'complex_')
    for mm in range(L):     
        U[mm,:]= U[mm,:] + np.roll(ro[-L:], mm)
 
    U= np.triu(U, 0)
    U_h= np.matrix.transpose(U)
    U_h= np.tril( U_h, -1)

    C_v= U + np.conjugate(U_h)  # Matriz de correlación

    # Descomponemos C_v y encontramos la matriz de blanqueo

    H= np.linalg.cholesky(C_v) # Descomposición de Cholesky

    W= np.linalg.inv(H) # Matriz de transformación de blanqueo

    X= np.dot(W, V) # Datos blanqueados

    # Salida

    return(X, C_v)